export const homescreen = {
	navigate() {
		cy.visit('http://parabank.parasoft.com')
	},
	login_username: 'chleba5',
	login_password: 'chleba5',
	incorrect_username: 'fgsfdhgs',
	incorrect_password: 'šž345šřžř',

	getCorrectUsername() {
		return cy.get('input[name="username"]')
		.type(homescreen.login_username)
	},
	getCorrectPassword() {
		return cy.get('input[name="password"]')
		.type(homescreen.login_password)
	},
	getLoginButton() {
		return cy.get(".button")
		.last()
		.wait(1000)
		.click()
		.wait(1000)
	},
};