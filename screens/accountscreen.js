export const accountscreen = {
	getPayeeInfo() {
		cy.get('input[name="payee.name"]')
			.type('Pan Prdel')
			.get('input[name="payee.address.street"]')
			.type('V Prdeli 123')
			.get('input[name="payee.address.city"]')
			.type('Prdelov')
			.get('input[name="payee.address.state"]')
			.type('Prdelistan')
			.get('input[name="payee.address.zipCode"]')
			.type('404E0R')
			.get('input[name="payee.phoneNumber"]')
			.type('123456789')
			.get('input[name="payee.accountNumber"]')
			.type('987654321')
			.get('input[name="verifyAccount"]')
			.type('987654321');
	},
};