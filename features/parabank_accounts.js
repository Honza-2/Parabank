describe("Parabank - Operace s Účty", () => {
	// 	Kontext: Jsem přihlášený na stránce parabank.parasoft.com
	beforeEach(() => {
		cy.visit("http://parabank.parasoft.com")
			.get("#bodyPanel")
			.find(".input")
			.first()
			.type("chleba")
			.get("#bodyPanel") // jak to udelat aby se to nemuselo opakovat? 
			.find(".input")
			.last()
			.type("chleba")
			.get(".button")
			.last()
			.click();
	})
	it("Založení nového účtu", () => {
		// 		Když kliknu na tlačítko založit nový účet v sekci "Account Services"
		cy.get("#leftPanel")
			.contains("Open New Account")
			.click();
		// 	A vyberu z drop-down menu typ účtu 
		cy.get("#type")
			.select("SAVINGS")
			.should("have.value", "1");
		// 	A vyberu z drop-down menu existující účet pro první deposit
		cy.get("#fromAccountId")
			.select("14787")
			.should("have.value", "14787");
		// A kliknu na tlačítko založit nový účet v sekci "Open New Account"
		cy.get(".button")
			.contains("Open New Account")
			.click();
	})
})