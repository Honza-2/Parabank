#language: cs
Požadavek: Přihlášení

	Kontext: 
		Pokud Jsem na stránce parabank.parasoft.com

	Scénář: 1. Validní přihlášení
		Když vyplním validní uživatelské jméno a heslo
		A kliknu na tlačítko login
		Pak je uživatel přihlášen
		A zobrazí se stránka Accounts Overview

	Scénář: 2. Přihlášení s nevalidními údaji
		Když vyplním nevalidní uživatelské jméno a heslo
		A kliknu na tlačítko login
		Pak se zobrazí hláška "The username and password could not be verified."

	Scénář: 3. Nevyplněné přihlašovací údaje
		Když nevyplním pole pro uživatelské jméno a heslo a kliknu na tlačítko login
		Pak se zobrazí hláška "Please enter a username and password."