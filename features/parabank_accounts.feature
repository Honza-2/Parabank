#language: cs
Požadavek: Operace s účty

	Kontext: 
		Pokud Jsem přihlášený na stránce parabank.parasoft.com

	Scénář: 1. Založení nového účtu
		Když kliknu na tlačítko založit nový účet v sekci "Account Services"
		A vyberu z drop-down menu typ účtu 
		A vyberu z drop-down menu existující účet pro první deposit
		A kliknu na tlačítko založit nový účet v sekci "Open New Account"
		Pak se zobrazí hláška účet byl založen
		A zobrazí se číslo nového účtu

	Scénář: 2. Převod peněz mezi vlastními účty
		Když kliknu na tlačítko převést peníze
		A do pole částka napíšu částku
		A vyberu účet z dropdown menu výchozí účet
		A vyberu účet z dropdown menu cílový účet
		A kliknu na tlačítko převést
		Pak se zobrazí hláška převod dokončen

	Scénář: 3. Platba faktury
		Když kliknu na tlačítko "Bill Pay"
		A vyplním validní údaje o příjemci
		A vyplním částku
		A vyberu účet z kterého chci odeslat platbu
		A kliknu na tlačítko odeslat platbu
		Pak se zobrazí hláška platba za fakturu byla úspěšně dokončena