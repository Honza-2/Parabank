describe("Parabank - login", () => {

	it("Validní přihlášení", () => {
		// Když vyplním validní údaje pro uživatelské jméno a heslo
		cy.visit("http://parabank.parasoft.com")
			// .get("#bodyPanel")
			// .find(".input")
			// .first()
			.get('input[name="username"]')
			.type("chleba")
			.get("#bodyPanel") // jak to udelat aby se to nemuselo opakovat? 
		

			.get('input[name="password"]')
			.type("chleba")
			// A kliknu na tlačítko login
			cy.get(".button")
			.last()	
			.click();
			// Pak je uživatel přihlášen
			// A zobrazí se stránka Accounts Overview
	})

	it("Přihlášení s nevalidními údaji", () => {
		//Když vyplním nevalidní uživatelské jméno a heslo
		cy.visit("http://parabank.parasoft.com")
		.get("#bodyPanel")
		.find(".input")
		.first()
		.type("chleba")
		.get("#bodyPanel")
		.find(".input")
		.last()
		.type("žžžžžžžžž")
		.get(".button")
		.last()
		.click();
	})

	it("Nevyplněné přihlašovací údaje", () => {
		//Když nevyplním pole pro uživatelské jméno a heslo a kliknu na tlačítko login
		cy.visit("http://parabank.parasoft.com")
		.get(".button")
		.last()
		.click();
	})
})