#language: cs
Požadavek: Registrace nového uživatele

	Kontext: 
		Pokud jsem na stránce parabank.parasoft.com a kliknu na tlačítko "Register" 

	Scénář: 1. Validní registrace
		Když vyplním validně všechny údaje
		A kliknu na tlačítko "REGISTER" umístěné pod tabulkou se zadanými údaji
		Pak je nový uživatel zaregistrován
		A rovnou přihlášen do systému
		A zobrazí se hláška "Your account was created successfully. You are now logged in."

	Scénář: 2. Nekompletní údaje pro registraci
		Když vyplním jen některé údaje
		A kliknu na tlačítko "REGISTER" pod tabulkou se zadanými údaji
		Pak se u chybějícího údaje zobrazí hláška "*chybějící údaj* is required"
	