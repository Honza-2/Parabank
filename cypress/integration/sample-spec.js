/// <reference types="Cypress" />



describe('My First Test', () => {
  it('clicking "type" shows the right headings', () => {
    cy.visit('https://example.cypress.io');

    // cy.pause()

    cy.contains('type').click();

    // Should be on a new URL which includes '/commands/actions'
    cy.url().should('include', '/commands/actions');

    // Get an input, type into it and verify that the value has been updated
    cy.get('.action-email')
      .type('fake@email.com')
      .should('have.value', 'fake@email.com');
  })

	it('Úspěšné přihlášení', () => {
		// Pokud jsem na stránce login.szn.cz
		cy.visit('http://login.szn.cz');
		// Když vyplním uživatelské jméno Koláč
		cy.get('#login-username')
			.type('kolac');
		// A vyplním heslo Test1234
		cy.get('#login-password')
			.type('Test1234');
		// A kliknu na tlačítko Přihlásit se
		cy.get('button')
			.first()
			.click();
		// Pak mě aplikace úspěšně přihlásí
	})
})


cy.get('div').should(($div) => {
  expect($div).to.have.length(1)

  const className = $div[0].className

  // className will be a string like "main-abc123 heading-xyz987"
  expect(className).to.match(/heading-/)
})