import {homescreen} from '../screens/homescreen.js'

describe("Parabank - login", () => {

	it("Validní přihlášení", () => {
		homescreen.navigate();
		// Když vyplním validní údaje pro uživatelské jméno a heslo
		homescreen.getCorrectUsername();
		// A vyplním heslo
		homescreen.getCorrectPassword();
		// A kliknu na tlačítko login
		cy.pause();
		homescreen.getLoginButton();
		// Pak je uživatel přihlášen a zobrazí se stránka Accounts Overview
		cy.get('h1')
			.contains('Accounts Overview');
	})

	it("Přihlášení s nevalidními údaji", () => {
		homescreen.navigate();
		//Když vyplním nevalidní uživatelské jméno a heslo
		cy.get('input[name="username"]')
			.type(homescreen.incorrect_username);
		// A vyplním heslo
		cy.get('input[name="password"]')
			.type(homescreen.incorrect_password);
		// A kliknu na tlačítko login
		homescreen.getLoginButton();
		// Pak se zobrazí hláška "The username and password could not be verified."
		cy.get('p')
			.contains('The username and password could not be verified.');
	// V aplikaci se najednou lze prihlasit i s nevalidnimi udaji, test failuje
	})

	it("Nevyplněné přihlašovací údaje", () => {
		// Když nevyplním pole pro uživatelské jméno a heslo a kliknu na tlačítko login
		cy.visit("http://parabank.parasoft.com")
		.get(".button")
		.last()
		.click();
		// Pak se zobrazí hláška "Please enter a username and password."
		cy.get('p')
			.contains('Please enter a username and password.')
	})
});