const { homescreen } = require("../screens/homescreen");
import {accountscreen} from '../screens/accountscreen';

describe("Parabank - Operace s Účty", () => {
	// 	Pokud jsem přihlášený na stránce parabank.parasoft.com
	beforeEach(() => {
		homescreen.navigate();
		homescreen.getCorrectUsername();
		homescreen.getCorrectPassword();
		homescreen.getLoginButton();
	});

	it("Založení nového účtu", () => {
		// 		Když kliknu na tlačítko založit nový účet v sekci "Account Services"
		cy.get('a')
			.contains("Open New Account")
			.click();
		// 	A vyberu z drop-down menu typ účtu 
		cy.get('#type')
			.select("SAVINGS")
			.should("have.value", "1");
		// 	A vyberu z drop-down menu existující účet pro první deposit
		cy.get('#fromAccountId')
			.select(0)
			.wait(10000);
			//.should("have.value", "13899");
		// A kliknu na tlačítko založit nový účet v sekci "Open New Account"
		cy.get("input.button")
			.contains("Open New Account")
			.click()
			.wait(10000);
		// Pak se zobrazí hláška "Congratulations, your account is now open."
		cy.wait(1000)
			.get('p')
			.contains('Congratulations, your account is now open.');
	});

	it('Převod peněz mezi vlastními účty', () => {
		// Když kliknu na tlačítko převést peníze
		cy.get('a')
			.contains('Transfer Funds')
			.click();
		// A do pole částka napíšu částku
	//	cy.pause(); // proč to bez pauzy nefunguje?? 
		cy.wait(1000)
			.get('#amount')
			.type('69');

		// A vyberu účet z dropdown menu výchozí účet
		cy.get('#fromAccountId')
			.select(0);
		// A vyberu účet z dropdown menu cílový účet
		cy//.get('select')
			.get('#toAccountId')
			.select(1);
		// A kliknu na tlačítko převést
		cy.get('input.button')
			.should('have.value', 'Transfer')
			.first()
			.click();
		// Pak se zobrazí hláška převod dokončen 
		cy.get('.ng-scope')
			.contains('Transfer Complete!');
	});

	it('Platba faktury', () => {
		// Když kliknu na tlačítko "Bill Pay"
		cy.get('a')
			.contains('Bill Pay')
			.click();
		// A vyplním validní údaje o příjemci
			accountscreen.getPayeeInfo();
			// A vyplním částku
			cy.get('input[name="amount"]')
			.type('69');
			// A vyberu účet z kterého chci odeslat platbu
			cy.get('select[name="fromAccountId"')
				.select(1);
			// A kliknu na tlačítko odeslat platbu
			cy.get('input.button[value="Send Payment"]')
				.click();
			// Pak se zobrazí hláška platba za fakturu byla úspěšně dokončena
			cy.wait(1000)
				.get('div[ng-show="showResult"]')
				.contains('Bill Payment Complete');
	})
});